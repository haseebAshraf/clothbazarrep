﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entites
{
    public class Product : BaseEntites
    {
    //    public int ID { get; set; }
    //    public string  Name { get; set; }
    //    public string Description { get; set; }
        public decimal Price { get; set; }
        public Category Category { get; set; }  // show relationship bw produt and category

    }
}
